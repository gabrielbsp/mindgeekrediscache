﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MndGk.Domain.Models
{
    public class VideoAlternativeDTO
    {
        public string Url { get; set; }
        public string Quality { get; set; }
    }
}
