﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MndGk.Domain.Models
{
    public class MovieDTO
    {
        public string Id { get; set; }
        public string Body { get; set; }
        public string Cert { get; set; }
        public string Class { get; set; }
        public int Duration { get; set; }
        public string Headline { get; set; }
        public string Quote { get; set; }
        public int Rating { get; set; }
        public DateTime LastUpdated { get; set; }
        public string ReviewAuthor { get; set; }
        public string SkyGoId { get; set; }
        public string SkyGoUrl { get; set; }
        public string Sum { get; set; }
        public string Synopsis { get; set; }
        public string Url { get; set; }
        public string Year { get; set; }
        public List<ImageDTO> CardImages { get; set; }
        public List<PersonDTO> Cast { get; set; }
        public List<PersonDTO> Directors { get; set; }
        public List<string> Genres { get; set; }
        public List<VideoDTO> Videos { get; set; }
        public List<ImageDTO> KeyArtImages { get; set; }
        public ViewWindowDTO ViewWindow { get; set; }

    }
}
