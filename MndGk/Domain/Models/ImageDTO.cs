﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MndGk.Domain.Models
{
    public class ImageDTO
    {
        public string Url { get; set; }
        public string H { get; set; }
        public string W { get; set; }
    }
}
