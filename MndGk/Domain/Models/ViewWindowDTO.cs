﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MndGk.Domain.Models
{
    public class ViewWindowDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string WayToWatch { get; set; }
    }
}
