﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MndGk.Domain.Models
{
    public class VideoDTO
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public List<VideoAlternativeDTO> VideoAlternatives { get; set; }
    }
}
