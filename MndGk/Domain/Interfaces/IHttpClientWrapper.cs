﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MndGk.Domain.Interfaces
{
    public interface IHttpClientWrapper
    {
        Task<string> GetAsStringAsync(string url);
    }
}
