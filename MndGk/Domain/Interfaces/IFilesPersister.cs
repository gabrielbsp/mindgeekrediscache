﻿using System.Threading.Tasks;

namespace MndGk.Domain.Interfaces
{
    public interface IFilesPersister
    {
        Task<bool> PersistFileAsync(string path, byte[] file);
    }
}
