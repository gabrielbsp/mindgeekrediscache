﻿using MndGk.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MndGk.Domain.Interfaces
{
    public interface ICacheDataService
    {
        Task<List<MovieDTO>> GetMovies(string url);
    }
}
