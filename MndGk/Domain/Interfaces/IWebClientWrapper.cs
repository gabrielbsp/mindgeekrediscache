﻿using System.Threading.Tasks;

namespace MndGk.Domain.Interfaces
{
    public interface IWebClientWrapper
    {
        Task DownloadFileTaskAsync(string address, string fileName);
    }
}
