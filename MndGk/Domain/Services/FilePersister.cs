﻿using MndGk.Domain.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;

namespace MndGk.Domain.Services
{
    public class FilePersister : IFilesPersister
    {
        public async Task<bool> PersistFileAsync(string path, byte[] file)
        {
            try
            {
                await using var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                await fileStream.WriteAsync(file, 0, file.Length);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
