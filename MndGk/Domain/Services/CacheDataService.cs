﻿using MndGk.Domain.Interfaces;
using MndGk.Domain.Models;
using Newtonsoft.Json;
using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MndGk.Domain.Services
{
    public class CacheDataService : ICacheDataService, IDisposable
    {
        private readonly string host = "localhost";
        private readonly string moviesCacheKey = "moviesCacheKey";
        private readonly IRedisTypedClient<List<MovieDTO>> cacheClient;
        private readonly IFilesPersister _filesPersister;
        private readonly IWebClientWrapper _webClient; // wrapped them for unit tests
        private readonly IHttpClientWrapper _httpClient;
        public CacheDataService(IFilesPersister filesPersister, 
            IWebClientWrapper webClient,
            IHttpClientWrapper httpClient,
            IRedisTypedClient<List<MovieDTO>> redisTypedClient = null) // added like this for unit tests
        {
            this._webClient = webClient;
            this._httpClient = httpClient;
            this._filesPersister = filesPersister;
            if(redisTypedClient != null)
            {
                this.cacheClient = redisTypedClient;
            }
            else
            {
                this.cacheClient = new RedisTypedClient<List<MovieDTO>>(new RedisClient(host));
            }
        }

        private void DeleteCachedImageFiles()
        {
            var cardImagesFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "cache", "images", "cardImages");
            var keyArtImagesFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "cache", "images", "keyArtImages");
            try
            {
                if (Directory.Exists(cardImagesFolderPath))
                {
                    var directoryInfoCardImages = new DirectoryInfo(cardImagesFolderPath);
                    foreach (var file in directoryInfoCardImages.GetFiles())
                    {
                        file.Delete();
                    }
                }
                if (Directory.Exists(keyArtImagesFolderPath))
                {
                    var directoryInfoKeyArt = new DirectoryInfo(keyArtImagesFolderPath);
                    foreach (var file in directoryInfoKeyArt.GetFiles())
                    {
                        file.Delete();
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }


        public async Task<List<MovieDTO>> GetMovies(string url)
        {
            var moviesFromCache = cacheClient.GetValue(moviesCacheKey);

            if (moviesFromCache != null)
            {
                return moviesFromCache;
            }

            DeleteCachedImageFiles();

            // if we have no cache then we'll have to download and cache the data
            var moviesJson = await DownloadMoviesData(url);

            var deserializedMovies = DeserializeMoviesJSON(moviesJson);

            await CacheMoviesDataAsync(deserializedMovies);

            return deserializedMovies;
        }

        private async Task CacheMoviesDataAsync(List<MovieDTO> movieDTOs)
        {

            foreach (var movieDto in movieDTOs)
            {
                await CacheMovieImagesAndUpdatePocoAsync(movieDto);
            }
            try
            {
                cacheClient.SetValue(moviesCacheKey, movieDTOs, TimeSpan.FromMinutes(10));
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Caches valid images from DTO obj and saves them on disk as static files, and updates the link to the image inside the object 
        /// with the path to the file on the server where the image is cached on disk
        /// so the image will be served from the server as a static file, not distributed throughout memory, thus saving processing power at retrieval
        /// </summary>
        /// <param name="movieDTO"></param>
        private async Task CacheMovieImagesAndUpdatePocoAsync(MovieDTO movieDTO)
        {
            var defaultPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "cache", "images");
            var keyArtImageFolder = Directory.CreateDirectory($@"{defaultPath}\keyArtImages\");
            var cardImageFolder = Directory.CreateDirectory($@"{defaultPath}\cardImages\");

            await HandleMovieImageCollection(movieDTO.CardImages, cardImageFolder.FullName, "cardImages");
            await HandleMovieImageCollection(movieDTO.KeyArtImages, keyArtImageFolder.FullName, "keyArtImages");

            RemoveFaultedImagesFromCollection(movieDTO.CardImages);
            RemoveFaultedImagesFromCollection(movieDTO.KeyArtImages);
        }

        // remove images that are not served from our static file cache
        private void RemoveFaultedImagesFromCollection(List<ImageDTO> images)
        {
            images.RemoveAll(x => !x.Url.Contains("localhost"));
        }


        private async Task HandleMovieImageCollection(List<ImageDTO> images, string fullPathImageFolder, string imageFolderName)
        {
            foreach (var img in images)
            {
                var filename = img.Url.Substring(img.Url.LastIndexOf('/') + 1);
                try
                {
                    await _webClient.DownloadFileTaskAsync(img.Url, $@"{fullPathImageFolder}\{filename}");
                    img.Url = "https://localhost:5001/cache/images/" + imageFolderName + "/" + filename;
                }
                catch (Exception e)
                {
                }
            }

        }

        private List<MovieDTO> DeserializeMoviesJSON(string moviesJson)
        {
            List<MovieDTO> moviesDtoList;

            try
            {
                moviesDtoList = JsonConvert.DeserializeObject<List<MovieDTO>>(moviesJson);
            }
            catch (Exception e)
            {
                throw;
            }

            return moviesDtoList;
        }

        private async Task<string> DownloadMoviesData(string url)
        {
            return await _httpClient.GetAsStringAsync(url);
        }

        public void Dispose()
        {
            DeleteCachedImageFiles();
        }
    }

}
