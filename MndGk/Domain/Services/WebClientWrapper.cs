﻿using MndGk.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MndGk.Domain.Services
{
    public class WebClientWrapper : IWebClientWrapper
    {
        private readonly WebClient _webClient = new WebClient();
        public Task DownloadFileTaskAsync(string address, string fileName)
        {
            return _webClient.DownloadFileTaskAsync(address, fileName);
        }
    }
}
