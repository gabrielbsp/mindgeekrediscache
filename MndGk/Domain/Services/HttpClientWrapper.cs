﻿using MndGk.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MndGk.Domain.Services
{
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly HttpClient httpClient = new HttpClient();
        public async Task<string> GetAsStringAsync(string url)
        {
            var response = await httpClient.GetAsync(url);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
