﻿using MndGk.Domain.Interfaces;
using MndGk.Domain.Models;
using MndGk.Domain.Services;
using Moq;
using ServiceStack.Redis.Generic;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MndGk.Tests
{
    public class CacheDataServiceTests
    {
        [Fact]
        public void ShouldReturnFromCacheIfNotEmpty()
        {
            // arrange (mocking dependencies)
            var filePersisterMock = new Mock<IFilesPersister>();
            var iredisMock = new Mock<IRedisTypedClient<List<MovieDTO>>>();
            var webClientMock = new Mock<IWebClientWrapper>();
            var httpClientMock = new Mock<IHttpClientWrapper>();


            iredisMock.Setup(x => x.GetValue(It.IsAny<string>())).Returns(new List<MovieDTO>());
            
            var cacheDataService = new CacheDataService(filePersisterMock.Object, 
                webClientMock.Object, httpClientMock.Object, iredisMock.Object);
            
            // act 
            var result = cacheDataService.GetMovies("mockUrl");

            // Assert
            Assert.NotNull(result);
            iredisMock.Verify(x => x.SetValue(It.IsAny<string>(), It.IsAny<List<MovieDTO>>()), Times.Never());
        }



        [Fact]
        public void ShouldSaveToCacheOnceIfCacheIsEmpty()
        {
            // arrange (mocking dependencies)
            var filePersisterMock = new Mock<IFilesPersister>();
            var iredisMock = new Mock<IRedisTypedClient<List<MovieDTO>>>();
            var webClientMock = new Mock<IWebClientWrapper>();
            var httpClientMock = new Mock<IHttpClientWrapper>();

            List<MovieDTO> nullRef = null;

            iredisMock.Setup(x => x.GetValue(It.IsAny<string>())).Returns(nullRef);

            webClientMock.Setup(x => x.DownloadFileTaskAsync(It.IsAny<string>(), It.IsAny<string>()));


            var httpMockResponse = new HttpResponseMessage();

            httpClientMock.Setup(x => x.GetAsStringAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(mockJsonString));
            
            var cacheDataService = new CacheDataService(filePersisterMock.Object,
                webClientMock.Object, httpClientMock.Object, iredisMock.Object);

            // act 
            
            var result = cacheDataService.GetMovies("mockUrl");

            // Assert
            Assert.NotNull(result);
            iredisMock.Verify(x => x.SetValue(It.IsAny<string>(), It.IsAny<List<MovieDTO>>(), It.IsAny<TimeSpan>()), Times.Once());
        }

        private readonly string mockJsonString = "[{\"id\":\"8ad589013b496d9f013b4c0b684a4a5d\",\"body\":\"Billy Crystal plays put-out-to-graze baseball commentator Artie who reluctantly pitches up with his indulgent wife Diane(Midler) at the home of their daughter Alice(Tomei).\nThey've been cajoled into looking after their grand-kids - brainy 12-year-old Harper (Madison), stuttering wall-flower Turner (Rush) and the youngest, pocket rebel Barker (Breitkopf).\nWhile Alice and her gizmo-obsessed husband Artie (Everett Scott) head off for a few days at a resort, it's up to Artie and Diane - who hardly know their grandchildren - to hold the fort and nurture the nippers.\nA few bright lines crop up in this tired slave to formula but it's a largely dull affair relying on two stars who were once very much of the moment - from Throw Momma From The Train (Crystal) to Beaches (Midler) - but now seem to have lost their spark.\nThere's the expected pratfalls - Barker tinkling on skateboard legend Tony Hawke, a half-hearted food fight and a family bonding session during a game of kick-the-can in a rainstorm but in an era of Modern Family excellence it's pretty thin gruel.\nThings reach a low when Artie vomits in a child's face.\nFace it Billy, this isn't that funny.\",\"cert\":\"U\",\"class\":\"Movie\",\"duration\":5940,\"headline\":\"Parental Guidance\",\"quote\":\"an intriguing pairing of Bette Midler and Billy Crystal\",\"rating\":3,\"lastUpdated\":\"2013-07-15T00:00:00+03:00\",\"reviewAuthor\":\"Tim Evans\",\"skyGoId\":\"d1bf901693832410VgnVCM1000000b43150a____\",\"skyGoUrl\":\"http://go.sky.com/vod/content/GOPCMOVIES/RSS/Movies/content/assetId/6ba3fb6afd03e310VgnVCM1000000b43150a________/videoId/d1bf901693832410VgnVCM1000000b43150a________/content/playSyndicate.do\",\"sum\":\"66b14d5c58904900b13b404ae29eb7fe\",\"synopsis\":\"When veteran baseball commentator Artie Decker (Billy Crystal) gets his cards the last thing he wants to do is child-mind three grand-kids he hardly knows. It doesn't help that he and his wife Diane (Bette Midler) have no truck with 21st century parenting methods and haven't got a clue how to operate their daughter's hi-tech house's gadgets. Chaos ensues... but life lessons are learned in this raucous family comedy.\",\"url\":\"http://skymovies.sky.com/parental-guidance/review\",\"year\":\"2012\",\"cardImages\":[{\"url\":\"https://localhost:5001/cache/images/cardImages/Parental-Guidance-VPA.jpg\",\"h\":\"1004\",\"w\":\"768\"},{\"url\":\"https://localhost:5001/cache/images/cardImages/LPA-Parental-guidance-LPA-to-LP4.jpg\",\"h\":\"600\",\"w\":\"960\"},{\"url\":\"https://localhost:5001/cache/images/cardImages/Parental-Guidance-DI-DI-to-CW.jpg\",\"h\":\"720\",\"w\":\"1280\"}],\"cast\":[{\"name\":\"Billy Crystal\"},{\"name\":\"Bette Midler\"},{\"name\":\"Marisa Tomei\"},{\"name\":\"Bailee Madison\"},{\"name\":\"Madison Lintz\"}],\"directors\":[{\"name\":\"Andy Fickman\"}],\"genres\":[\"Comedy\",\"Family\"],\"videos\":[{\"url\":\"http://static.video.sky.com//skymovies/2012/11/44104/44104-360p_800K_H264.mp4\",\"title\":\"Trailer - Parental Guidance\",\"type\":\"trailer\",\"videoAlternatives\":null},{\"url\":\"http://proxy.video.sky.com/video/clip-0085plin\",\"title\":\"Parental Guidance: Trailer\",\"type\":\"trailer\",\"videoAlternatives\":null}],\"keyArtImages\":[{\"url\":\"https://localhost:5001/cache/images/keyArtImages/Parental-Guidance-KA-KA-to-KP3.jpg\",\"h\":\"169\",\"w\":\"114\"}],\"viewWindow\":null}]";
    }
}
