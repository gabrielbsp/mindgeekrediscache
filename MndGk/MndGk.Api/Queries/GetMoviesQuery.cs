﻿using MediatR;
using MndGk.Api.Models;

namespace MndGk.Api.Queries
{
    public class GetMoviesQuery : IRequest<GetMoviesQueryResult>
    {
        public GetMoviesQuery(string url)
        {
            this.Url = url;
        }
        public string Url { get; set; }
    }
}
