﻿using MediatR;
using MndGk.Api.Models;
using MndGk.Api.Queries;
using MndGk.Domain.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace MndGk.Api.QueryHandlers
{
    public class GetMoviesQueryHandler : IRequestHandler<GetMoviesQuery, GetMoviesQueryResult>
    {
        private readonly ICacheDataService _cacheDataService;
        public GetMoviesQueryHandler(ICacheDataService cacheDataService)
        {
            _cacheDataService = cacheDataService;
        }
        public async Task<GetMoviesQueryResult> Handle(GetMoviesQuery request, CancellationToken cancellationToken)
        {
            var movies = await _cacheDataService.GetMovies(request.Url);
            var result = new GetMoviesQueryResult(movies);
            return result;
        }
    }

}
