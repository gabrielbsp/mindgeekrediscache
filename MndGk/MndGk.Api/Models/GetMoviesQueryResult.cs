﻿using MndGk.Domain.Models;
using System.Collections.Generic;

namespace MndGk.Api.Models
{
    public class GetMoviesQueryResult
    {
        public GetMoviesQueryResult(List<MovieDTO> movies)
        {
            Movies = movies;
        }
        public List<MovieDTO> Movies { get; set; }
    }
}
