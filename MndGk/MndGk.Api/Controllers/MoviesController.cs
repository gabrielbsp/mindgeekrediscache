﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MndGk.Api.Queries;
using System.Threading.Tasks;

namespace MndGk.Api.Controllers
{
    [ApiController]
    [Route("api/movies")]
    public class MoviesController : ControllerBase
    {
        private readonly IMediator _mediator;
        public MoviesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _mediator.Send(new GetMoviesQuery("https://mgtechtest.blob.core.windows.net/files/showcase.json"));
            return Ok(result);
        }
    }
}
