# README #
 

### This Web Api exposes api/movies endpoint ( Http GET) 
### this flow does the following : 
*  Step1 : reads the JSON data from JSON
*  Step2 : serializes the data into POCO
*  Step3 : caches (on disk) all available image assets found inside the POCOs
*  Step4 : removes unavailable/corrupted images from the movies list
*  Step5 : and replaces their url with the path to the static file served directly from the web api 
*  Step6 : caches in redis the POCOs obtained 
*  Step7 : After expiry time passed (now set at 10minutes)
*  Step8 : Upon receiveing a new request it deletes current unmanaged memory (image assets in this case)
*  Step9 : And repeats from 1.


### Frameworks used: 

* redis-64
* .net core 3.1 web api 

### How do I get set up? ###

* open powershell command window as administrator

* run these commands : 

* (installs chocolatey)
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

* (installs redis)
```
choco install redis-64 
```
* (runs redis)
```
redis-server
```
* run  to make sure redis server is running
```
redis-cli ping
```
* then build and run the web project
 